﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstructuraDeDatos
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {

        }

        private void memoramaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadMemorama cambio = new loadMemorama();
            cambio.Show();
            this.Hide();
        }

        private void triangularToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTriangular cambio = new frmTriangular();
            cambio.Show();
            this.Hide();
        }

        private void practica1ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void sumaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSumaMatrices cambio = new frmSumaMatrices();
            cambio.Show();
            this.Hide();
        }

        private void inversaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMatrizInversa cambio = new FrmMatrizInversa();
            cambio.Show();
            this.Hide();
        }

        private void transpuestaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMatrizTranspuesta cambio = new frmMatrizTranspuesta();
            cambio.Show();
            this.Hide();
        }

        private void metodosDeOrdenamientoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void evaluacionDeExpresionesPosfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmInfoNotacionPostFija cambio = new FrmInfoNotacionPostFija ();
            cambio.Show();
            this.Hide();
        }

        private void pilaGenericaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPilaGenerica cambio = new FrmPilaGenerica();
            cambio.Show();
            this.Hide();
        }

        private void torresDeHanoiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTorresDeHanoi cambio = new FrmTorresDeHanoi();
            cambio.Show();
            this.Hide();
        }

        private void cuadroMagicoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tiposDeDatosAbstractosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTipoDeDatos cambio = new frmTipoDeDatos();
            cambio.Show();
            this.Hide();
        }

        private void fibonacciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void pruebaDeFibonacciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRecursividad cambio = new frmRecursividad();
            cambio.Show();
            this.Hide();
        }
    }
}
