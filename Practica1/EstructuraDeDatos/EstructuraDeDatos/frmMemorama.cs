﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace EstructuraDeDatos
{
    public partial class frmMemorama : Form
    {
        PictureBox imgTemporal;
        int posicionTemporal, intentos, pares, segundos, minutos = 0;
        int restantes = 8;
        bool esperando = true;
        
        string ruta = @"C:\Users\planc\Desktop\EstructuraDeDatos\Practica1\EstructuraDeDatos\EstructuraDeDatos\Resources\img\";

        string[] posiciones = {"1.jpg", "1.jpg" , "2.jpg" , "2.jpg" ,
                               "3.jpg" , "3.jpg" , "4.jpg" , "4.jpg",
                               "5.jpg","5.jpg","6.jpg","6.jpg","7.jpg",
                               "7.jpg","8.jpg","8.jpg"
                               };
        public void voltear(PictureBox img, int posicion)
        {
            try
            {
                timer1.Enabled = true;
                img.BackgroundImage = Image.FromFile(ruta + posiciones[posicion]);
                img.Update();
                if (esperando)
                {
                    imgTemporal = img;
                    posicionTemporal = posicion;
                    img.Enabled = false;
                }
                else
                {
                    intentos0ToolStripMenuItem.Text = "Intentos: "+(++intentos);
                    if (posiciones[posicionTemporal]==posiciones[posicion])
                    {
                        imgTemporal.Visible = false;
                        img.Visible = false;
                        imgTemporal.BackgroundImage = null;
                        img.BackgroundImage = null;
                        imgTemporal.Enabled = false;
                        img.Enabled = false;
                        pares0ToolStripMenuItem.Text = "Pares: " + (++pares);
                        restantes0ToolStripMenuItem.Text = "Restantes: " + (--restantes);
                        SoundPlayer sonido = new SoundPlayer();
                        sonido.SoundLocation = "C:/Users/planc/Desktop/EstructuraDeDatos/Practica1/EstructuraDeDatos/EstructuraDeDatos/Resources/song/telaraña.wav";
                        sonido.Play();
                        if (pares==8)
                        {
                            timer1.Enabled = false;
                            DialogResult opcion;
                            opcion=MessageBox.Show("FELICIDADES HAS GANADO!!"+"\n" + "Tus intentos fueron: "+intentos+ "\n"+"Tu tiempo fue: "+minutos+":"+segundos+"\n","JUEGO TERMINADO",MessageBoxButtons.OKCancel,MessageBoxIcon.Question);
                            if (opcion==DialogResult.OK)
                            {
                                frmMenu menu = new frmMenu();
                                menu.Show();
                                this.Close();
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        img.BackgroundImage = Image.FromFile(ruta + "tapa1.jpg");
                        imgTemporal.BackgroundImage = Image.FromFile(ruta + "tapa1.jpg");
                        imgTemporal.Enabled = true;
                        img.Enabled = true;
                    }
                }
                esperando =! esperando;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public frmMemorama()
        {
            InitializeComponent();
        }

        private void frmMemorama_Load(object sender, EventArgs e)
        {
            intentos0ToolStripMenuItem.Text = "Intentos: 0"; 
            intentos = 0;
            pares0ToolStripMenuItem.Text = "Pares: 0";
            pares = 0;
            timer1.Enabled = false;
            timpoRestante000ToolStripMenuItem.Text = "Tiempo Restante: 0:00 ";
            
            Random random = new Random();
            int deck = random.Next(1,4);
            ruta = @"C:\Users\planc\Desktop\EstructuraDeDatos\Practica1\EstructuraDeDatos\EstructuraDeDatos\Resources\img\deck"+deck+@"\";
           
            int fondo = random.Next(1, 11);
            this.BackgroundImage = Image.FromFile(@"C:\Users\planc\Desktop\EstructuraDeDatos\Practica1\EstructuraDeDatos\EstructuraDeDatos\Resources\img\fondos\"+fondo+".jpg");
            
            posiciones = posiciones.OrderBy(s => Guid.NewGuid()).ToArray();
            foreach (PictureBox item in this.Controls.OfType<PictureBox>())
            {
                item.BackgroundImage = Image.FromFile(ruta + "tapa1.jpg");
                item.BackgroundImageLayout = ImageLayout.Stretch;
                item.Visible = true;
                item.Enabled = true;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            voltear(pictureBox1, 0);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            voltear(pictureBox2, 1);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            voltear(pictureBox3, 2);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            voltear(pictureBox4, 3);

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            voltear(pictureBox5, 4);

        }


        private void pictureBox6_Click(object sender, EventArgs e)
        {
            voltear(pictureBox6, 5);

        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            voltear(pictureBox7, 6);

        }
        private void pictureBox8_Click(object sender, EventArgs e)
        {
            voltear(pictureBox8, 7);

        }
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            voltear(pictureBox9, 8);

        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            voltear(pictureBox10, 9);

        }
        private void pictureBox11_Click(object sender, EventArgs e)
        {
            voltear(pictureBox11, 10);

        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            voltear(pictureBox12, 11);

        }


        private void pictureBox13_Click(object sender, EventArgs e)
        {
            voltear(pictureBox13, 12);

        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            voltear(pictureBox14, 13);

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void pictureBox15_Click(object sender, EventArgs e)
        {
            voltear(pictureBox15, 14);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            segundos++;
            if (segundos==60)
            {
                minutos++;
                segundos = 0;
            }
            timpoRestante000ToolStripMenuItem.Text ="Tiempo Jugado: " + minutos + " : " + segundos;

        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            voltear(pictureBox16, 15);

        }

        private void reiciarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMemorama_Load(sender, e);
            
        }
    }
}
