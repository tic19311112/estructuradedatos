﻿namespace EstructuraDeDatos
{
    partial class frmMenu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.menu = new System.Windows.Forms.MenuStrip();
            this.unidadConceptosBásicosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiposDeDatosAbstractosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recursividadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mCDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.busquedaBinariasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pruebaDeFibonacciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fractalDeHilbertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unidad2ArreglosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sumaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inversaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.triangularToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transpuestaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoramaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unidad3ListasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unidad4PilasYColasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluacionDeExpresionesPosfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unidad5ArbolesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Imagen1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pilaGenericaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.torresDeHanoiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Imagen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unidadConceptosBásicosToolStripMenuItem,
            this.unidad2ArreglosToolStripMenuItem,
            this.unidad3ListasToolStripMenuItem,
            this.unidad4PilasYColasToolStripMenuItem,
            this.unidad5ArbolesToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(841, 24);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // unidadConceptosBásicosToolStripMenuItem
            // 
            this.unidadConceptosBásicosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tiposDeDatosAbstractosToolStripMenuItem,
            this.recursividadToolStripMenuItem});
            this.unidadConceptosBásicosToolStripMenuItem.Name = "unidadConceptosBásicosToolStripMenuItem";
            this.unidadConceptosBásicosToolStripMenuItem.Size = new System.Drawing.Size(171, 20);
            this.unidadConceptosBásicosToolStripMenuItem.Text = "Unidad 1: Conceptos básicos";
            // 
            // tiposDeDatosAbstractosToolStripMenuItem
            // 
            this.tiposDeDatosAbstractosToolStripMenuItem.Name = "tiposDeDatosAbstractosToolStripMenuItem";
            this.tiposDeDatosAbstractosToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.tiposDeDatosAbstractosToolStripMenuItem.Text = "Tipos de datos abstractos";
            this.tiposDeDatosAbstractosToolStripMenuItem.Click += new System.EventHandler(this.tiposDeDatosAbstractosToolStripMenuItem_Click);
            // 
            // recursividadToolStripMenuItem
            // 
            this.recursividadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mCDToolStripMenuItem,
            this.busquedaBinariasToolStripMenuItem,
            this.pruebaDeFibonacciToolStripMenuItem,
            this.fractalDeHilbertToolStripMenuItem});
            this.recursividadToolStripMenuItem.Name = "recursividadToolStripMenuItem";
            this.recursividadToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.recursividadToolStripMenuItem.Text = "Recursividad";
            // 
            // mCDToolStripMenuItem
            // 
            this.mCDToolStripMenuItem.Name = "mCDToolStripMenuItem";
            this.mCDToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.mCDToolStripMenuItem.Text = "MCD";
            // 
            // busquedaBinariasToolStripMenuItem
            // 
            this.busquedaBinariasToolStripMenuItem.Name = "busquedaBinariasToolStripMenuItem";
            this.busquedaBinariasToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.busquedaBinariasToolStripMenuItem.Text = "Busqueda Binarias";
            // 
            // pruebaDeFibonacciToolStripMenuItem
            // 
            this.pruebaDeFibonacciToolStripMenuItem.Name = "pruebaDeFibonacciToolStripMenuItem";
            this.pruebaDeFibonacciToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pruebaDeFibonacciToolStripMenuItem.Text = "Recursividad";
            this.pruebaDeFibonacciToolStripMenuItem.Click += new System.EventHandler(this.pruebaDeFibonacciToolStripMenuItem_Click);
            // 
            // fractalDeHilbertToolStripMenuItem
            // 
            this.fractalDeHilbertToolStripMenuItem.Name = "fractalDeHilbertToolStripMenuItem";
            this.fractalDeHilbertToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.fractalDeHilbertToolStripMenuItem.Text = "Fractal de Hilbert";
            // 
            // unidad2ArreglosToolStripMenuItem
            // 
            this.unidad2ArreglosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sumaToolStripMenuItem,
            this.inversaToolStripMenuItem,
            this.triangularToolStripMenuItem,
            this.transpuestaToolStripMenuItem,
            this.memoramaToolStripMenuItem});
            this.unidad2ArreglosToolStripMenuItem.Name = "unidad2ArreglosToolStripMenuItem";
            this.unidad2ArreglosToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.unidad2ArreglosToolStripMenuItem.Text = "Unidad 2: Arreglos";
            // 
            // sumaToolStripMenuItem
            // 
            this.sumaToolStripMenuItem.Name = "sumaToolStripMenuItem";
            this.sumaToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.sumaToolStripMenuItem.Text = "Suma";
            this.sumaToolStripMenuItem.Click += new System.EventHandler(this.sumaToolStripMenuItem_Click);
            // 
            // inversaToolStripMenuItem
            // 
            this.inversaToolStripMenuItem.Name = "inversaToolStripMenuItem";
            this.inversaToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.inversaToolStripMenuItem.Text = "Inversa";
            this.inversaToolStripMenuItem.Click += new System.EventHandler(this.inversaToolStripMenuItem_Click);
            // 
            // triangularToolStripMenuItem
            // 
            this.triangularToolStripMenuItem.Name = "triangularToolStripMenuItem";
            this.triangularToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.triangularToolStripMenuItem.Text = "Triangular Y Determinante";
            this.triangularToolStripMenuItem.Click += new System.EventHandler(this.triangularToolStripMenuItem_Click);
            // 
            // transpuestaToolStripMenuItem
            // 
            this.transpuestaToolStripMenuItem.Name = "transpuestaToolStripMenuItem";
            this.transpuestaToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.transpuestaToolStripMenuItem.Text = "Transpuesta";
            this.transpuestaToolStripMenuItem.Click += new System.EventHandler(this.transpuestaToolStripMenuItem_Click);
            // 
            // memoramaToolStripMenuItem
            // 
            this.memoramaToolStripMenuItem.Name = "memoramaToolStripMenuItem";
            this.memoramaToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.memoramaToolStripMenuItem.Text = "Memorama";
            this.memoramaToolStripMenuItem.Click += new System.EventHandler(this.memoramaToolStripMenuItem_Click);
            // 
            // unidad3ListasToolStripMenuItem
            // 
            this.unidad3ListasToolStripMenuItem.Name = "unidad3ListasToolStripMenuItem";
            this.unidad3ListasToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.unidad3ListasToolStripMenuItem.Text = "Unidad 3: Listas";
            // 
            // unidad4PilasYColasToolStripMenuItem
            // 
            this.unidad4PilasYColasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evaluacionDeExpresionesPosfToolStripMenuItem,
            this.pilaGenericaToolStripMenuItem,
            this.torresDeHanoiToolStripMenuItem});
            this.unidad4PilasYColasToolStripMenuItem.Name = "unidad4PilasYColasToolStripMenuItem";
            this.unidad4PilasYColasToolStripMenuItem.Size = new System.Drawing.Size(137, 20);
            this.unidad4PilasYColasToolStripMenuItem.Text = "Unidad 4: Pilas y Colas";
            // 
            // evaluacionDeExpresionesPosfToolStripMenuItem
            // 
            this.evaluacionDeExpresionesPosfToolStripMenuItem.Name = "evaluacionDeExpresionesPosfToolStripMenuItem";
            this.evaluacionDeExpresionesPosfToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.evaluacionDeExpresionesPosfToolStripMenuItem.Text = "Evaluacion de expresiones postfijas";
            this.evaluacionDeExpresionesPosfToolStripMenuItem.Click += new System.EventHandler(this.evaluacionDeExpresionesPosfToolStripMenuItem_Click);
            // 
            // unidad5ArbolesToolStripMenuItem
            // 
            this.unidad5ArbolesToolStripMenuItem.Name = "unidad5ArbolesToolStripMenuItem";
            this.unidad5ArbolesToolStripMenuItem.Size = new System.Drawing.Size(112, 20);
            this.unidad5ArbolesToolStripMenuItem.Text = "Unidad 5: Arboles";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Yu Gothic UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "Competencias:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Yu Gothic UI", 18F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(34, 273);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(286, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "Objetivo De Aprendizaje:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Yu Gothic UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(57, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(400, 105);
            this.label3.TabIndex = 4;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Yu Gothic UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(57, 317);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(387, 84);
            this.label4.TabIndex = 5;
            this.label4.Text = "El alumno empleará estructuras de datos abstractas \r\nen el desarrollo de aplicaci" +
    "ones multiplataforma \r\nusando el paradigma orientado a objetos para \r\nagilizar e" +
    "l acceso a los datos.";
            // 
            // Imagen1
            // 
            this.Imagen1.Image = global::EstructuraDeDatos.Properties.Resources._1_Ut55XNszCQPxCG9qaEQfAw;
            this.Imagen1.Location = new System.Drawing.Point(478, 169);
            this.Imagen1.Name = "Imagen1";
            this.Imagen1.Size = new System.Drawing.Size(351, 242);
            this.Imagen1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Imagen1.TabIndex = 7;
            this.Imagen1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::EstructuraDeDatos.Properties.Resources._14;
            this.pictureBox1.Location = new System.Drawing.Point(-586, 92);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(409, 343);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Yu Gothic UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(56, 468);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(313, 25);
            this.label5.TabIndex = 9;
            this.label5.Text = "Ramón Francisco Rivera Valenzuela";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Yu Gothic UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(56, 503);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(247, 25);
            this.label6.TabIndex = 10;
            this.label6.Text = "José Humberto Abril Garcia";
            // 
            // pilaGenericaToolStripMenuItem
            // 
            this.pilaGenericaToolStripMenuItem.Name = "pilaGenericaToolStripMenuItem";
            this.pilaGenericaToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.pilaGenericaToolStripMenuItem.Text = "Pila Generica";
            this.pilaGenericaToolStripMenuItem.Click += new System.EventHandler(this.pilaGenericaToolStripMenuItem_Click);
            // 
            // torresDeHanoiToolStripMenuItem
            // 
            this.torresDeHanoiToolStripMenuItem.Name = "torresDeHanoiToolStripMenuItem";
            this.torresDeHanoiToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.torresDeHanoiToolStripMenuItem.Text = "Torres de Hanoi";
            this.torresDeHanoiToolStripMenuItem.Click += new System.EventHandler(this.torresDeHanoiToolStripMenuItem_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(841, 537);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Imagen1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estructura De Datos Aplicada";
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Imagen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem unidadConceptosBásicosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unidad2ArreglosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unidad3ListasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unidad4PilasYColasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unidad5ArbolesToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox Imagen1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripMenuItem tiposDeDatosAbstractosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recursividadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mCDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busquedaBinariasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pruebaDeFibonacciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fractalDeHilbertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sumaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inversaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transpuestaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoramaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem triangularToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluacionDeExpresionesPosfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pilaGenericaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem torresDeHanoiToolStripMenuItem;
    }
}

