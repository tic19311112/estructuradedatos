﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstructuraDeDatos
{
    public partial class frmSumaMatrices : Form
    {
        Random random = new Random();
        public frmSumaMatrices()
        {
            InitializeComponent();
        }

        private void bntGenerar_Click(object sender, EventArgs e)
        {
            for(int i=0; i<int.Parse(txtColumnas.Text); i++)
            {
                dataGridView1.Columns.Add("colum"+i,"colum"+i);
                dataGridView2.Columns.Add("colum" + i, "colum" + i);
                dataGridView3.Columns.Add("colum" + i, "colum" + i);
            }

            for (int i = 0; i < int.Parse(txtRenglones.Text); i++)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[i].HeaderCell.Value = "Row" + i;
                dataGridView2.Rows.Add();
                dataGridView2.Rows[i].HeaderCell.Value = "Row" + i;
                dataGridView3.Rows.Add();
                dataGridView3.Rows[i].HeaderCell.Value = "Row" + i;
            }

            for (int c = 0; c < int.Parse(txtColumnas.Text); c++)
            {
                for (int r = 0; r < int.Parse(txtRenglones.Text); r++)
                {
                    dataGridView1[c, r].Value = "1";
                    dataGridView2[c, r].Value = "1";
              
                }
            }

        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            for (int c = 0; c < int.Parse(txtColumnas.Text); c++)
            {
                for (int r = 0; r < int.Parse(txtRenglones.Text); r++)
                {
                   
                dataGridView3[c, r].Value = int.Parse(dataGridView1[c, r].Value.ToString()) + int.Parse(dataGridView2[c, r].Value.ToString());

                }
            }
        }

        private void btnRandom_Click(object sender, EventArgs e)
        {
            for (int c = 0; c < int.Parse(txtColumnas.Text); c++)
            {
                for (int r = 0; r < int.Parse(txtRenglones.Text); r++)
                {
                    
                    dataGridView1[c, r].Value = random.Next(int.Parse(txtLimInferior.Text)-1, int.Parse(txtLimExterior.Text)+1).ToString();
                    dataGridView2[c, r].Value = random.Next(int.Parse(txtLimInferior.Text) - 1, int.Parse(txtLimExterior.Text) + 1).ToString();

                }
            }
        }

        private void frmSumaMatrices_Load(object sender, EventArgs e)
        {

        }
    }
}
